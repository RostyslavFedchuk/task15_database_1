#1
select model from pc where model LIKE "%1%1%";
#2
select * from Outcome where month(`date`) = 03;
#3
select * from  Outcome_o where day(`date`) = 14;
#4
select `name` from Ships where `name` like "W%n";
#5
select `name` from Ships where `name` RLIKE "^[^e]*e[^e]*e[^e]*$";
#6
select `name`, launched from ships where `name` not like "%a";
#7
select * from Outcomes where battle RLike "[^ ]+ .+[^c]$";
#8
select * from Trip where hour(time_out) between 12 and 17;
#9
select * from Trip where hour(time_in) between 17 and 23;
#10
select date from Pass_in_trip where place like "1%";
#11
select date from Pass_in_trip where place like "%c";
#12
select name from Passenger where name like "_% C_%";
#13
select name from Passenger where name not like "_% J_%";