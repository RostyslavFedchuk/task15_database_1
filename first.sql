use labor_sql;
#1
SELECT maker, `type` FROM product WHERE `type` = 'Laptop' ORDER BY maker;
#2
SELECT model, ram, screen, price FROM Laptop WHERE price>1000 ORDER BY ram DESC, price ASC;
#3
SELECT * FROM Printer WHERE color != 'n' ORDER BY price DESC;
#4
select model, speed, hd, cd, price from pc where (cd = "12x" or cd = "24x") and price< 600 Order by speed DESC;
#5
select `name`, class from Ships order by `name`;
#6
select * from pc where speed >= 500 and price<800 order by price desc;
#7
select * from Printer where type != 'Matrix' and price<300 order by type desc;
#8
select  model, speed from pc where price between 400 and 600 order by hd;
#9
select pc.model, speed, hd from pc, product where pc.model = product.model and (hd=10 or hd=20) and product.maker = 'A' order by speed ;
#10
select model, speed, hd, price from Laptop where screen>=12 order by price desc;
#11
select model, type, price from Printer where price < 300 order by type desc;
#12
select model, ram, price from laptop where ram = 64 order by screen;
#13
select model, ram, price from pc where ram > 64 order by hd;
#14
select model, speed, price from pc where speed between 500 and 700 order by hd desc;
#15
select * from outcome_o where `out` > 2000 order by date desc;
#16
select  * from income_o where inc between 5000 and 10000 order by inc;
#17
select  * from income where point = 1 order by inc;
#18
select  * from outcome where point = 2 order by `out`;
#19
select * from classes where country="Japan" order by `type` desc;
#20
select name, launched from Ships where launched between 1920 and 1945 order by launched desc;
#21
select ship, battle, result from Outcomes where battle="Guadalcanal" and result!='sunk' order by ship desc;
#22
select ship, battle, result from outcomes where result="sunk" order by ship desc;
#23
select class, displacement from classes where displacement >= 40 order by type;
#24
select trip_no, town_from, town_to from trip where town_to = 'London' or town_from = 'London' order by time_out; 
#25
select  trip_no, plane, town_from, town_to from trip where plane='TU-134' order by time_out desc;
#26
select trip_no, plane, town_from, town_to from trip where plane= "IL-86" order by plane;
#27
select  trip_no, town_from, town_to from trip where town_from!='Rostov' AND town_to!='Rostov' order by plane;
