#1
select maker, type, speed, hd from pc,product where pc.model = product.model and hd <= 8;
#2
select maker from pc,product where pc.model = product.model and speed >=600;
#3
select maker from laptop, product where laptop.model = product.model and `type`= 'laptop' and speed <=500;
#4
select distinct L1.model, L2.model, L1.hd, L1.ram from laptop L1 inner join laptop L2 on L1.code != L2.code and L1.ram = L2.ram and L1.hd = L2.hd 
order by L1.model desc, L2.model;
#5
select country from classes where `type` = 'bc' and country IN (select country from classes where`type` = 'bb');
#6
select pc.model, maker from product inner join pc on product.model = pc.model where price < 600;
#7
select printer.model, maker from product inner join printer on product.model = printer.model where price > 300; 
#8
select maker, pc.model, price from pc left join product on pc.model = product.model;
#9
select  maker, pc.model, price from pc left join product on pc.model = product.model where price is not NULL;
#10
select maker, type, laptop.model, speed from laptop left join product on laptop.model = product.model where speed > 600;
#11
select name, displacement from Ships S inner join Classes C on S.class = C.class;
#12
select name, date from Outcomes O inner join Battles B on O.battle = B.name where result!="sunk";
#13
select name, country from Ships S inner join Classes C on S.class = C.class;
#14
select trip_no, plane, name from Trip T inner join Company C on T.ID_comp = C.ID_comp where plane = 'Boeing';
#15
select name, date from Passenger P inner join Pass_in_trip PIT on P.ID_psg = PIT.ID_psg;